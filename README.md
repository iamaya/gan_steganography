# GAN_Steganography

Project for **CSCI 6365 - Network Management and Security**.

Use a **Generative Adversarial Network (GAN)** to encrypt and conceal an image for transmission.