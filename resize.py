# resize pokeGAN.py
import os
import cv2

src = "./images"  # original images
dst = "./resizedImages"  # resized images

os.mkdir(dst)

for image in os.listdir(src):
    img = cv2.imread(os.path.join(src, image))
    img = cv2.resize(img,(256,256))
    cv2.imwrite(os.path.join(dst, image), img)